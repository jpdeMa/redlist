//
//  MapView.swift
//  RedList
//
//  Created by Jean-Pierre on 04/05/2020.
//  Copyright © 2020 Jean-Pierre. All rights reserved.
//

import SwiftUI
import MapKit

struct MapView: UIViewRepresentable {
    
    func makeUIView(context: Context) -> MKMapView {
        MKMapView(frame: .zero)
    }
    
    func updateUIView(_ mapView: MKMapView, context: Context) {
        let coordinate = CLLocationCoordinate2D(
            latitude: 31.69423,
            longitude: 85.536248)
        let span = MKCoordinateSpan(latitudeDelta: 12, longitudeDelta: 12)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
