//
//  ContentView.swift
//  RedList
//
//  Created by Jean-Pierre on 04/05/2020.
//  Copyright © 2020 Jean-Pierre. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        VStack {
            MapView()
                .frame(height: 300)
                .edgesIgnoringSafeArea(.top)
            CircleImage()
                .offset(y: -150)
                .padding(.bottom, -100)
            
            //
            VStack(alignment: .leading) {
                Text("Mamal")
                HStack {
                    Text("Snow Leopard")
                        .font(.title)
                    Spacer()
                    Text("Asia")
                }
                HStack {
                    Text("Decreasing")
                    Text("VU")
                    Text("2700 - 3300")
                }
            }
            .padding()
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
